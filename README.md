# 2223-Incidencias

Proyecto gestión de incidencias. Gestión de proyectos, Universidad de Málaga

## Estructura del proyecto

```text
2223-incidencias
├── doc/                # Documentación y entregables
├── glpi/               # Código fuente para la aplicación de gestión de incidencias
├── portfolio/          # Código fuente para el portfolio personal
├── resources           # Recursos varios de soporte
├── INSTALL.md          # Instrucciones de despliegue
├── README.md
└── docker-compose.yml
```
