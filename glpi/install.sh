#!/bin/bash

# Exit on failure. Print commands
set -ex

GLPI_HOME=/var/www/html/glpi
SOURCES=/sources

echo 'Adding footer...'
cp "$SOURCES/src/Html.php" "$GLPI_HOME/src/Html.php" 
chmod 777 "$GLPI_HOME/src/Html.php"
echo "Sucess"

echo "Adding logo..."
cp "$SOURCES/src/css_palettes_auror.min.css" "$GLPI_HOME/css_compiled/css_palettes_auror.min.css"
cp "$SOURCES/logos/logo-Farmacia-100.png" "$GLPI_HOME/pics/logos/logo-Farmacia-100.png"
chmod 777 "$GLPI_HOME/css_compiled/css_palettes_auror.min.css"
echo "Sucess"

echo "Removing install.php"
rm "$GLPI_HOME/install/install.php" && echo "Sucess"

echo "Done!"