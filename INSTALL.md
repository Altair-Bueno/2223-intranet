Este documento sirve como guía de instalación para el servicio de incidencias

# Requisitos

- [Docker y Docker compose](https://docs.docker.com/compose/install/)
- [Git](https://git-scm.com/)

# Pasos para la instalación

La instalación del servicio ha sido probada en un sistema operativo macOS 12 y
Ubuntu 22.04

## Obtener el código fuente

El repositorio de código contiene los ficheros necesarios para realizar la
instalación del servicio. Será necesario descargar dichos ficheros mediante la
herramienta _git_

```sh
git clone https://gitlab.com/Altair-Bueno/2223-intranet.git
cd 2223-intranet
```

## Desplegar GLPi y MariaDB

En la raíz del proyecto se proporciona un fichero `docker-compose.yml` con toda
la información necesaria para desplegar una instancia de GLPi completamente
funcional, así como los servicios adicionales necesarios (MariaDB)

```sh
docker compose up -d
```

## Instalación de GLPi

Para completar la instalación de GLPi, será necesario acceder al portal web de
GLPi. Dicho portal estará disponible en `http://IP_MACHINE`, donde `IP_MACHINE`
se corresponde con la dirección IP de la máquina donde estamos realizando la
instalación.

Será necesario continuar con la instalación de GLPI introduciendo los siguientes
datos

```yml
Idioma de instalación: Español (España)
servidor SQL: mariadb
usuario SQL: glpi_user
contraseña SQL: glpi
Seleccione una base de datos: glpidb
```

![1. Selección del idioma](resources/choose-language.png)

![2. Aceptar la licencia](resources/accept-license.png)

![3. Instalar](resources/install.png)

![4. Continuar](resources/choose-install-options.png)

![5. Configuración de la conexión a la base de datos](resources/2022-12-04-12-17-40.png)

![6. Seleccionar la base de datos](resources/2022-12-04-12-18-41.png)

![7. Base de datos inicializada](resources/2022-12-04-12-19-58.png)

![8. Desactivar el envió de estadísticas de uso](resources/2022-12-04-12-20-23.png)

![9. Terminar setup](resources/2022-12-04-12-20-53.png)

![10. Iniciar GLPI](resources/2022-12-04-12-21-15.png)

# Modificar GLPI

Tras terminar la instalación de GLPI, será necesario acceder manualmente al
contenedor y crear los ficheros necesarios para completar la instalación. La
instalación está automatizada por medio de un script, por lo que solo será
necesario comprobar si dicho script termina satisfactoriamente

```sh
# Acceder al contenedor
docker exec -it glpi bash
/sources/install.sh
# Salir del contenedor
exit
```

# Verificar la instalación

Para verificar la instalación, bastará con recargar la página de inicio de
sesión de GLPI y comprobar que tanto el logo como el footer son iguales a la
siguiente captura

![Login GLPi](resources/2022-12-04-12-27-10.png)

# Errores comunes de la instalación

## No aparece el logo

Esto es causado por la caché del navegador. Para solucionarlo, es necesario
borrar los datos de la página web actual y recargar. Las instrucciones para
varios navegadores se pueden encontrar en los siguientes enlaces

- [Google Chrome](https://support.google.com/accounts/answer/32050?hl=en&co=GENIE.Platform%3DDesktop)
- [Firefox](https://support.mozilla.org/en-US/kb/how-clear-firefox-cache)
- [Safari](https://support.apple.com/en-us/HT201265)
